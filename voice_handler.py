from pydub import AudioSegment
from telegram import Update
import speech_recognition as sr

from constants import *
from user_commands import reply_message


def reply_recognized_text(update: Update) -> None:
    voice = update.message.voice
    voice_path = get_download_path(voice)
    download_voice(voice, voice_path)
    wav_path = get_convert_path(voice)
    convert_ogg_to_wav(voice_path, wav_path)
    recognized_text = recognize_sphinx_audio(wav_path)
    reply_message("Сфинкс думает, что тут сказано: \n" + recognized_text, update)


def get_download_path(voice):
    voice_path = VOICE_AUDIO_PATH + voice.file_unique_id + ".ogg"
    return voice_path


def get_convert_path(voice):
    convert_path = CONVERTED_AUDIO_PATH + voice.file_unique_id + ".wav"
    return convert_path


def download_voice(voice, voice_path):
    voice.get_file().download(voice_path)


def convert_ogg_to_wav(input_path, output_path):
    audio = AudioSegment.from_ogg(input_path)
    audio.export(output_path, format='wav')


def recognize_sphinx_audio(wav_path):
    recognizer = sr.Recognizer()
    with sr.AudioFile(wav_path) as source:
        audio = recognizer.record(source)

    try:
        recognized_text = recognizer.recognize_sphinx(audio, language="ru")
        return recognized_text
    except sr.UnknownValueError:
        print("Sphinx could not understand audio")
    except sr.RequestError as e:
        print("Sphinx error; {0}".format(e))
