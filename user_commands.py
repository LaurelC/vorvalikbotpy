import random

from constants import *
from telegram import Update
from telegram.ext import CallbackContext


def reply_message(text, update: Update) -> None:
    update.message.reply_text(text, reply_to_message_id=update.message.message_id)


def on_help_user_command(update: Update, context: CallbackContext) -> None:
    message = "Я Ворвалик, и я умею здороваться!"
    reply_message(message, update)


def on_hello_user_command(update: Update, context: CallbackContext) -> None:
    message = random.choice(HELLOS) + ", " + update.message.from_user.first_name
    reply_message(message, update)
