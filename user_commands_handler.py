from user_commands import *

from voice_handler import reply_recognized_text


def on_text_message(update: Update, context: CallbackContext) -> None:
    if update.message.text == HELLO_COMMAND:
        on_hello_user_command(update, context)
    if update.message.text == HELP_COMMAND:
        on_help_user_command(update, context)


def on_voice_message(update: Update, context: CallbackContext) -> None:
    reply_recognized_text(update)
