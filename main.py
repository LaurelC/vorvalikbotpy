from telegram.ext import Updater, MessageHandler, Filters

from user_commands_handler import on_text_message, on_voice_message
from constants import *


def main():
    updater = Updater(BOT_ACCESS_TOKEN)

    updater.dispatcher.add_handler(MessageHandler(Filters.text, on_text_message))
    updater.dispatcher.add_handler(MessageHandler(Filters.voice, on_voice_message))

    updater.start_polling()
    updater.idle()


if __name__ == '__main__':
    main()
